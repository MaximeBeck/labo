package mbe.labo.tf1_smartphone;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by maxime on 2/8/15.
 */
public class FileDeployer {
    private Context context;

    public FileDeployer(Context context) {
        this.context = context;
    }

    /**
     * Copie un fichier du dossier "assets" de l'APK au répertoire relatif à l'application
     * dans la mémoire externe de l'appareil.
     *
     * @param fname Nom du fichier
     * @param subFolder Sous-dossier dans le répertoire relatif à l'application
     */
    public void copyFromAssetToExternalMemory(String fname, String subFolder) {
        this.createFile(fname, getAssetFileContent(fname), subFolder);
    }

    /**
     * Retourne une phrase décrivant le niveau d'accessiblité de
     * la mémoire externe de l'apparail.
     *
     * @return
     */
    public String getExternalStorageStateAsString() {
        String text;

        if (this.isExternalStorageWritable())
            text = "External storage is writable.";
        else if (this.isExternalStorageReadable())
            text = "External storage is only readable.";
        else
            text = "External storage is unavailable.";

        return text;
    }

    /**
     * Vérifie si la mémoire externe de l'appareil est accessible
     * en écriture.
     *
     * @return
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie si la mémoire externe de l'appareil est lisible.
     *
     * @return
     */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


    /**
     * Création d'un fichier dans la mémoire externe de l'appareil
     *
     * @param fname Nom du fichier
     * @param content Contenu du fichier à crée
     * @param subFolder Sous-dossier dans le répertoire relatif à l'application
     */
    public void createFile(String fname,
                           String content, String subFolder) {
        File rootDir = new File(this.context.getExternalFilesDir(null), subFolder);
        rootDir.mkdirs(); // Crée l'arborescence en mémoire

        File file = new File(rootDir, fname);

        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);

            out.write(content.getBytes());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retourne le contenu d'un fichier situé dans le répertoire "assets" de l'APK.
     *
     * @param fname Nom du fichier
     * @return
     */
    public String getAssetFileContent(String fname) {
        BufferedReader br = null;
        String text = "";

        try {
            InputStream is = this.context.getAssets().open(fname);
            String UTF8 = "utf8";
            int BUFFER_SIZE = 8192;

            br = new BufferedReader(new InputStreamReader(is,
                    UTF8), BUFFER_SIZE);
            String str;
            while ((str = br.readLine()) != null) {
                text += str;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return text;
    }
}
