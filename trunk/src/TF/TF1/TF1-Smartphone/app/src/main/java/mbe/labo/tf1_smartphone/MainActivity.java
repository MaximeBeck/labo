package mbe.labo.tf1_smartphone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.Config;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends Activity implements CordovaInterface {
    private final ExecutorService threadPool = Executors.newCachedThreadPool();

    /**
     * Contrôle la WebView Cordova
     */
    CordovaWebView cordovaWebView;

    /**
     * Permet de déploiment des fichiers de l'application
     * JavaScript.
     */
    FileDeployer fileDeployer;

    /*
        --== Overrides liées à la classe "Activity" ==--
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Config.init(this);

        // Initialisation des composants
        cordovaWebView = (CordovaWebView) findViewById(R.id.wv_Cordova);
        fileDeployer = new FileDeployer(this);

        this.setButtonAvailability(Buttons.DEPLOY, true);
        this.setButtonAvailability(Buttons.LOAD, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
        --== Implémentation de CordovaInterface ==--
     */
    @Override
    public void startActivityForResult(CordovaPlugin command, Intent intent, int requestCode){

    }

    @Override
    public void setActivityResultCallback(CordovaPlugin plugin){

    }

    @Override
    public Activity getActivity(){
        return this;
    }


    @Override
    public Object onMessage(String id, Object data){
        return null;
    }

    @Override
    public ExecutorService getThreadPool(){
        return this.threadPool;
    }

    /*
        --== Gestion des évenements ==--
     */
    public enum Buttons {
        DEPLOY,
        LOAD
    }

    /**
     * Copie les fichiers index.html et action.js du dossier assets
     * dans le répértoire de la mémoire externe de l'appareil relatif
     * à l'application.
     *
     * @param view
     */
    public void deploy(View view) {
        TextView tvDeploy = (TextView) findViewById(R.id.tv_deploy);
        tvDeploy.setText(fileDeployer.getExternalStorageStateAsString() + "\n");

        this.setButtonAvailability(Buttons.DEPLOY, false);

        try {
            if(fileDeployer.isExternalStorageWritable()) {
                fileDeployer.copyFromAssetToExternalMemory("index.html", "www");
                fileDeployer.copyFromAssetToExternalMemory("action.js", "www/js");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setButtonAvailability(Buttons.DEPLOY, true);
        this.setButtonAvailability(Buttons.LOAD, true);

        tvDeploy.append("Files have been deployed\n");
    }

    public void setButtonAvailability(Buttons buttonToSet, Boolean enable) {
        Button btn = null;
        Boolean setEnable = true;

        switch(buttonToSet) {
            case DEPLOY:
                btn = (Button) findViewById(R.id.btn_deploy);
                break;

            case LOAD:
                btn = (Button) findViewById(R.id.btn_loadFiles);
                break;

            default:
                setEnable = false;
                break;
        }

        if(setEnable)
            btn.setEnabled(enable);
    }

    /**
     * Rafréchie la WebView Cordova en lui associant la bonne URL.
     *
     * @param view
     */
    public void loadFiles(View view) {
        File indexFile = new File(this.getExternalFilesDir(null) + "/www/index.html");
        cordovaWebView.loadUrl("file:///" + indexFile.getAbsolutePath());
    }
}