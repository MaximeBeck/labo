package mbe.labo.tf2_smartphone;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.luugiathuy.apps.remotebluetooth.R;

import java.util.List;

/**
 * Created by maxime on 5/24/15.
 */
public class BluetoothAPI {

    private static final int REQUEST_ENABLE_BT = 2;

    // Message Handler constantes
    private static final int MESSAGE_STATE_CHANGE = 10;
    private static final int MESSAGE_DEVICE_NAME = 11;
    private static final int MESSAGE_TOAST = 12;

    // Key names received from the BluetoothCommandService Handler
    public static final String DEVICE_NAME = "device_name";

    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for Bluetooth Command Service
    private BluetoothCommandService mCommandService = null;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Application context (main activity)
    private RemoteBluetooth context;

    private Handler mHandler;

    public BluetoothAPI(RemoteBluetooth context, Handler handler) {
        this.context = context;
        this.mHandler = handler;
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public boolean isBluetoothSupported() {
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null)
            return false;
        return true;
    }

    public void enableBlutooth() {
        // If BT is not on, request that it be enabled.
        // setupCommand() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            context.startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
        // otherwise set up the command service
        else {
            if (mCommandService == null)
                setupCommand();
        }
    }

    public void connect(String address) {
        // Get the BLuetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mCommandService.connect(device);
    }

    public void resumeConnection() {
        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mCommandService != null) {
            if (mCommandService.getState() == BluetoothCommandService.STATE_NONE) {
                mCommandService.start();
            }
        }
    }

    public void stopConnection() {
        if (mCommandService != null)
            mCommandService.stop();
    }

    public void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            context.startActivity(discoverableIntent);
        }
    }

    public void write(String message) {
        mCommandService.write(message);
    }

    public void write(Object obj) {
        mCommandService.write(obj);
    }

    public void write(byte[] data) {
        mCommandService.write(data);
    }

    public void write(int number) {
        mCommandService.write(number);
    }

    public void setupCommand() {
        // Initialize the BluetoothChatService to perform bluetooth connections
        mCommandService = new BluetoothCommandService(context, mHandler);
    }

}
