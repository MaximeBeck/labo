package mbe.labo.tf2_common;

import java.io.Serializable;

/**
 * Created by maxime on 3/13/15.
 */
public class Person implements Serializable {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
