package mbe.labo.tf2_common;

import java.io.Serializable;

/**
 * Created by ET on 25.05.2015.
 */
public class XappMessage implements Serializable {
    private String message;
    private String deviceAddress;

    public XappMessage(String deviceAddress, String message) {
        this.deviceAddress = deviceAddress;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }
}
