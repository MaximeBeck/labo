package mbe.labo.tf2_desktop;

import mbe.labo.tf2_common.XappMessage;
import mbe.labo.tf2_common.Person;
import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.bluetooth.RemoteDevice;
import javax.microedition.io.Connection;
import javax.microedition.io.StreamConnection;

public class ProcessConnectionThread implements Runnable{

    private StreamConnection mConnection;

	// Constant that indicate command from devices
	public static final int EXIT_CMD = -1;
    public static final int OBJECT = 0;
    public static final int STRING = 1;

    private InputStream inputStream;
    private RemoteDevice device;

    public ProcessConnectionThread(StreamConnection connection) throws IOException {
		mConnection = connection;
        device = RemoteDevice.getRemoteDevice((Connection)connection);
    }

	@Override
	public void run() {
        try {
            boolean running = true;

            inputStream = mConnection.openInputStream();
	        while (running) {

                int c = inputStream.read();

                switch(c) {
                    case OBJECT:
                        Object obj = this.readObject();

                        for(Object[] o : WaitThread.connectedDevices) {
                            if(!o[0].equals(device)) {
                                ProcessOutcomingThread pot = (ProcessOutcomingThread) o[1];
                                pot.write(obj);
                            }
                        }
                        break;
                    case STRING:
                        String message = this.readString();

                        System.out.println(device.getBluetoothAddress() + " : " + message);

                        for(Object[] o : WaitThread.connectedDevices) {
                            if(!o[0].equals(device)) {
                                ProcessOutcomingThread pot = (ProcessOutcomingThread) o[1];
                                XappMessage msg = new XappMessage(device.getBluetoothAddress(), message);
                                pot.write(msg);
                            }
                        }
                        break;
                    case EXIT_CMD:
                        try {
                            for(Object[] o : WaitThread.connectedDevices) {
                                if(o[0].equals(device)) {
                                    WaitThread.connectedDevices.remove(o);
                                }
                            }
                        } catch (Exception e) {
                            // Ne rien faire
                        }
                        running = false;
                        break;
                }
        	}
            if(WaitThread.connectedDevices.size() > 0) {
                for (Object[] o : WaitThread.connectedDevices) {
                    if (!o[0].equals(device)) {
                        ProcessOutcomingThread pot = (ProcessOutcomingThread) o[1];
                        pot.write(device.getBluetoothAddress() + " has been deconnected");
                    }
                }
            }
            System.out.println(device.getBluetoothAddress() + " has been deconnected");
        } catch (Exception e) {
    		e.printStackTrace();
    	}
	}

    /*
     *  *******************************************
     *  * INT  *    INT    *    BYTE     *  BYTE  *
     *  *******************************************
     *  * ENUM * TYPE_SIZE * OBJECT_SIZE * OBJECT *
     *  *******************************************
     */
    private Object readObject() throws IOException {
        List<Byte> bObject = new ArrayList<Byte>();
        List<Byte> bObjectSize = null;
        int typeSize = -1, objectSize = -1, c;
        boolean streaming = true;

        try {
            while (streaming) {
                c = inputStream.read();

                if(typeSize == -1) {
                    typeSize = c;
                }

                if(bObjectSize == null) {
                    bObjectSize = new ArrayList<Byte>();
                    for(int i = 0; i < typeSize; i++) {
                        c = inputStream.read();
                        bObjectSize.add(new Byte((byte) c));
                    }
                    Byte[] bytes = bObjectSize.toArray(new Byte[bObjectSize.size()]);
                    objectSize = ByteBuffer.wrap(ArrayUtils.toPrimitive(bytes)).getInt();
                }

                if (objectSize != -1) {
                    for(int i = 0; i < objectSize; i++) {
                        c = inputStream.read();
                        bObject.add(new Byte((byte) c));
                    }

                    streaming = false;
                }
            }
            Byte[] bytes = bObject.toArray(new Byte[bObject.size()]);

            return ByteArraySerializer.deserialize(ArrayUtils.toPrimitive(bytes));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String readString() throws IOException {
        boolean streaming = true;
        String strResult = "";
        int c;

        while (streaming) {
            int size = inputStream.read();

            for(int i = 0; i < size; i++) {
                c = inputStream.read();
                strResult += (char)c;
            }
            streaming = false;
            //String str = new String(bytes)
        }

        return strResult;
    }

    private void processCommand(Object obj) {
        try {
            Person p = (Person)obj;
            System.out.print("Nom " + p.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCommand(String str) {
        try {
            System.out.print("Received String : " + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCommand(byte[] data) {
        try {
            Person p = (Person)ByteArraySerializer.deserialize(data);
            System.out.println("Name : " + p.getName());
            System.out.println("Age : " + p.getAge());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}