package mbe.labo.tf2_desktop;

import sun.rmi.runtime.Log;

import javax.microedition.io.StreamConnection;
import javax.print.DocFlavor;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Scanner;

/**
 * Created by ET on 24.04.2015.
 */
public class ProcessOutcomingThread implements Runnable {
    public static final int OBJECT = 0;

    private StreamConnection mConnection;

    private OutputStream outputStream;

    public ProcessOutcomingThread(StreamConnection connection) {
        mConnection = connection;
    }

    @Override
    public void run() {
        try {
            outputStream = mConnection.openOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(byte[] buffer) {
        try {
            outputStream.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(int out) {
        try {
            outputStream.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String str) {
        byte[] b = str.getBytes();
        int nbBytesInString = str.length();

        write(ProcessConnectionThread.STRING);
        write(nbBytesInString);
        write(b);
    }

    public void write(Object obj) {
        byte[] bObj = ByteArraySerializer.serialize(obj);
        int nbBytesInObject = bObj.length;
        byte[] bNb = ByteBuffer.allocate(4).putInt(nbBytesInObject).array();
        int nbBytesInNb = bNb.length;

        write(OBJECT);
        write(nbBytesInNb);
        write(bNb);
        write(bObj);
    }
}
