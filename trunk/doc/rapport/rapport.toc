\select@language {french}
\contentsline {section}{\numberline {0.1}Pr\IeC {\'e}face}{1}
\contentsline {chapter}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {1.1}Description}{2}
\contentsline {section}{\numberline {1.2}Contexte}{2}
\contentsline {section}{\numberline {1.3}Objectifs}{4}
\contentsline {subsection}{\numberline {1.3.1}Phase 1 : Automatisation}{4}
\contentsline {subsection}{\numberline {1.3.2}Phase 2 : R\IeC {\'e}alisation}{4}
\contentsline {subsection}{\numberline {1.3.3}Phase 3 : Distribution}{4}
\contentsline {section}{\numberline {1.4}Projet de laboratoire}{4}
\contentsline {part}{I\hspace {1em}Th\IeC {\'e}orie}{5}
\contentsline {chapter}{\numberline {2}Choix des langages de programmation}{6}
\contentsline {section}{\numberline {2.1}Introduction}{6}
\contentsline {section}{\numberline {2.2}Application Desktop}{6}
\contentsline {section}{\numberline {2.3}Application Smartphone}{6}
\contentsline {chapter}{\numberline {3}Cordova}{8}
\contentsline {section}{\numberline {3.1}Introduction}{8}
\contentsline {section}{\numberline {3.2}Concepte de fonctionnement}{9}
\contentsline {chapter}{\numberline {4}Bluetooth}{11}
\contentsline {section}{\numberline {4.1}Introduction}{11}
\contentsline {section}{\numberline {4.2}Pourquoi Bluetooth ?}{11}
\contentsline {section}{\numberline {4.3}BlueCove}{12}
\contentsline {section}{\numberline {4.4}API Mobile}{12}
\contentsline {part}{II\hspace {1em}Pratique}{13}
\contentsline {chapter}{\numberline {5}Testes de faisabilit\IeC {\'e}s}{14}
\contentsline {section}{\numberline {5.1}Introduction}{14}
\contentsline {section}{\numberline {5.2}TF-1}{14}
\contentsline {subsection}{\numberline {5.2.1}Description}{14}
\contentsline {subsection}{\numberline {5.2.2}D\IeC {\'e}ploiment d'une Xapp}{16}
\contentsline {subsection}{\numberline {5.2.3}Chargement et ex\IeC {\'e}cution d'une Xapp}{16}
\contentsline {section}{\numberline {5.3}TF-2}{17}
\contentsline {subsection}{\numberline {5.3.1}Description}{17}
\contentsline {subsection}{\numberline {5.3.2}Activation de Bluetooth}{19}
\contentsline {subsection}{\numberline {5.3.3}Connexion \IeC {\`a} l'ordinateur}{19}
\contentsline {subsection}{\numberline {5.3.4}Envoi et r\IeC {\'e}ception de messages}{20}
\contentsline {chapter}{\numberline {6}Synth\IeC {\`e}se et conclusion}{22}
\contentsline {chapter}{\numberline {7}Webographie}{24}
